import java.util.Scanner;

public class Main {
    private static void findLongestPalindromicString(String text)
    {
        int N = 2 * text.length() + 1;

        if (N == 1)
            return;

        int[] longestPalindromLengthArr = new int[N + 1];
        longestPalindromLengthArr[0] = 0;
        longestPalindromLengthArr[1] = 1;
        int centerPointer = 1;
        int rightPointer = 2;
        int currentLeftPointer;
        int maxLPSLength = 0;
        int maxLPSCenterPosition = 0;
        int start = -1;
        int end = -1;
        int diff = -1;

        for (int i = 2; i < N; i++)
        {
            currentLeftPointer = 2 * centerPointer - i;
            longestPalindromLengthArr[i] = 0;
            diff = rightPointer - i;

            if (diff > 0)
                longestPalindromLengthArr[i] = Math.min(longestPalindromLengthArr[currentLeftPointer], diff);

            while (((i + longestPalindromLengthArr[i]) + 1 < N && (i - longestPalindromLengthArr[i]) > 0) &&
                    (((i + longestPalindromLengthArr[i] + 1) % 2 == 0) ||
                            (text.charAt((i + longestPalindromLengthArr[i] + 1) / 2) ==
                                    text.charAt((i - longestPalindromLengthArr[i] - 1) / 2))))
            {
                longestPalindromLengthArr[i]++;
            }

            if (longestPalindromLengthArr[i] > maxLPSLength) // Track maxLPSLength
            {
                maxLPSLength = longestPalindromLengthArr[i];
                maxLPSCenterPosition = i;
            }

            if (i + longestPalindromLengthArr[i] > rightPointer)
            {
                centerPointer = i;
                rightPointer = i + longestPalindromLengthArr[i];
            }
        }

        start = (maxLPSCenterPosition - maxLPSLength) / 2;
        end = start + maxLPSLength - 1;
        System.out.print("\nLPS of string is : ");
        for (int i = start; i <= end; i++)
            System.out.print(text.charAt(i));
        System.out.println("\n");
    }

    // Driver Code
    public static void main(String[] args)
    {
        boolean control = true;
        while(control) {
            Scanner sc= new Scanner(System.in);    //System.in is a standard input stream
            System.out.print("Please write exit for stop \n");
            System.out.print("Please enter a text: ");
            String text = sc.nextLine();
            if(text.equals("exit"))
                control = false;
            else
                findLongestPalindromicString(text); ;
        }

    }
}







